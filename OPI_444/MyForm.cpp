#include "MyForm.h"
using namespace OPI444;
using namespace System;
using namespace System::Windows::Forms;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Data;
using namespace System::Drawing;
[STAThread]
void main(array<System::String^>^ args) {
    System::Windows::Forms::Application::EnableVisualStyles();
    System::Windows::Forms::Application::SetCompatibleTextRenderingDefault(false);
    MyForm form;
    System::Windows::Forms::Application::Run(% form);
}

