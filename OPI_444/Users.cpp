#include "Users.h"
#include "MarshalString.h"
#include"BaseException.h"
#include <fstream>
#include <iostream>
#include <string>

Users::Users()
{
	std::ifstream fill("users.txt");
	while (fill)
	{
		char* login = new char[256];
		char* password = new char[256];
		char* firstName = new char[256];
		char* secondName = new char[256];
		char* group = new char[256];
		fill >> login;
		fill >> password;
		fill >> firstName;
		fill >> secondName;
		fill >> group;
		User user(login, password, firstName, secondName, group);
		m_users.push_back(user);
	}
	fill.close();
}

Users::~Users()
{
	try {
		if (m_users.size() > 0) {
			std::ofstream add("users.txt");
			for (User i : m_users)
			{
				add << i.GetLogin() << std::endl;
				add << i.GetPassword() << std::endl;
				add << i.GetFirstName() << std::endl;
				add << i.GetSecondName() << std::endl;
				add << i.GetGroup() << std::endl;
			}
			add.close();
		}
		else throw BaseException("Vector Users have no users");
	}
	catch (BaseException& exc) { std::cerr << "Base Exception: " << exc.GetError(); }
}

bool Users::CheckUser(System::String^ log, System::String^ pas)
{
	bool checked{};
	char* login{ MarshalString(log) }, * password{ MarshalString(pas) };
	for (User i : m_users)
	{
		if (strcmp(i.GetLogin(),login)==0&&strcmp(i.GetPassword(),password)==0)
		{
			checked = true;
			break;
		}
	}
	return checked;
}

void Users::AddUser(System::String^ log, System::String^ pas, System::String^ fN, System::String^ sN, System::String^gr)
{
	try{
		char* password = MarshalString(pas);
		if (strlen(password) < 5) throw BaseException("Password should be more then 4 symbols");
		if (!CheckUser(log, pas))
		{
			char* login = MarshalString(log);
			char* firstName = MarshalString(fN);
			char* secondName = MarshalString(sN);
			char* group = MarshalString(gr);
			m_users.push_back(User(login, password, firstName, secondName, group));
		}
		else throw BaseException("This user already exist");
	}
	catch(BaseException &exc){
		std::cerr << "Base exception: " << exc.GetError();
	}
}

System::String^ Users::GetName(System::String^ log)
{
	char* login = MarshalString(log);
	try {
		for (User i : m_users)
		{
			if (i.GetLogin() == login)
			{
				System::String^ name = gcnew System::String(i.GetFirstName());
				return name;
			}
		}
		throw BaseException("This User does not exist");
	}
	catch (BaseException& exc) { std::cerr << "Base exception: " << exc.GetError(); }
}
