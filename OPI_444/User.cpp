#define SECURE_NO_WARNINGS
#include "User.h"
#include <string>
#include <iostream>
//using namespace System;


User::User()
{
	m_login = new char[256];
	m_password = new char[256];
	m_firstName = new char[256];
	m_secondName = new char[256];
	m_group = new char[256];
}

User::User(char* log, char* pas, char* fN, char* sN, char* gr)
{
	m_login = new char[256];
	m_password = new char[256];
	m_firstName = new char[256];
	m_secondName = new char[256];
	m_group = new char[256];
	strcpy(m_login,log);
	strcpy(m_password, pas);
	strcpy(m_firstName, fN);
	strcpy(m_secondName, sN);
	strcpy(m_group, gr);
}

User::~User()
{}

User::User(const User& user)
{
	m_login = new char[256];
	m_password = new char[256];
	m_firstName = new char[256];
	m_secondName = new char[256];
	m_group = new char[256];
	strcpy(m_login, user.m_group);
	strcpy(m_password, user.m_password);
	strcpy(m_firstName, user.m_firstName);
	strcpy(m_secondName,user.m_secondName);
	strcpy(m_group, user.m_group);
}

char* User::GetLogin()
{
	return m_login;
}
char* User::GetPassword()
{
	return m_password;
}
char* User::GetFirstName()
{
	return m_firstName;
}
char* User::GetSecondName()
{
	return m_secondName;
}
char* User::GetGroup()
{
	return m_group;
}

std::istream& operator>>(std::istream& in, User& user)
{
	in >> user.m_login;
	in >> user.m_password;
	in >> user.m_firstName;
	in >> user.m_secondName;
	in >> user.m_group;
	return in;
}